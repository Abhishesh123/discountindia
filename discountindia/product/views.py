from rest_framework.response import Response
from rest_framework.views import APIView
from .serializer import *
from rest_framework import status


class MainProductApi(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MainProductSerializer(MainProduct.objects.all(), many=True)
            return Response(serializer.data)
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mainproduct': '',
                         'message': 'you are not authorized user'})


class MobileCompanies(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MobileCompaniesSerializer(Product.objects.filter(main_product__name__icontains='mobile'),
                                                   many=True)
            return Response({'status': status.HTTP_200_OK,
                             'mobilecompany': serializer.data,
                             'message': 'Successfully'})
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mobilecompany': '',
                         'message': 'you are not authorized user'})


class MobileCompaniesPhonesNew(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MobileCompaniesPhonesSerializer(
                SubPoduct.objects.filter(product__main_product__name__icontains='mobile', new=True), many=True)
            return Response({'status': status.HTTP_200_OK,
                             'mobilecompany': serializer.data,
                             'message': 'Successfully'})
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mobilecompanyphones': '',
                         'message': 'you are not authorized user'})


class MobileCompaniesPhonesPopular(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MobileCompaniesPhonesSerializer(
                SubPoduct.objects.filter(product__main_product__name__icontains='mobile', popular=True), many=True)
            return Response({'status': status.HTTP_200_OK,
                             'mobilecompany': serializer.data,
                             'message': 'Successfully'})
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mobilecompanyphones': '',
                         'message': 'you are not authorized user'})


class MobileCompaniesPhonesUpcoming(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MobileCompaniesPhonesSerializer(
                SubPoduct.objects.filter(product__main_product__name__icontains='mobile', upcoming=True), many=True)
            return Response({'status': status.HTTP_200_OK,
                             'mobilecompany': serializer.data,
                             'message': 'Successfully'})
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mobilecompanyphones': '',
                         'message': 'you are not authorized user'})


class LaptopCompanies(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MobileCompaniesSerializer(Product.objects.filter(main_product__name__icontains='laptop'),
                                                   many=True)
            return Response({'status': status.HTTP_200_OK,
                             'mobilecompany': serializer.data,
                             'message': 'Successfully'})
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mobilecompany': '',
                         'message': 'you are not authorized user'})


class NewLaptops(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MobileCompaniesPhonesSerializer(
                SubPoduct.objects.filter(product__main_product__name__icontains='laptop', new=True), many=True)
            return Response({'status': status.HTTP_200_OK,
                             'mobilecompany': serializer.data,
                             'message': 'Successfully'})
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mobilecompanyphones': '',
                         'message': 'you are not authorized user'})


class PopularLaptops(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MobileCompaniesPhonesSerializer(
                SubPoduct.objects.filter(product__main_product__name__icontains='laptop', popular=True), many=True)
            return Response({'status': status.HTTP_200_OK,
                             'mobilecompany': serializer.data,
                             'message': 'Successfully'})
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mobilecompanyphones': '',
                         'message': 'you are not authorized user'})


class UpcomingLaptops(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MobileCompaniesPhonesSerializer(
                SubPoduct.objects.filter(product__main_product__name__icontains='laptop', upcoming=True), many=True)
            return Response({'status': status.HTTP_200_OK,
                             'mobilecompany': serializer.data,
                             'message': 'Successfully'})
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mobilecompanyphones': '',
                         'message': 'you are not authorized user'})


class PopularTvs(APIView):
    def post(self, request):
        if AnonymousToken.objects.filter(key__exact=request.data['token']):
            serializer = MobileCompaniesPhonesSerializer(
                SubPoduct.objects.filter(product__main_product__name__icontains='tv', popular=True), many=True)
            return Response({'status': status.HTTP_200_OK,
                             'mobilecompany': serializer.data,
                             'message': 'Successfully'})
        return Response({'status': status.HTTP_401_UNAUTHORIZED,
                         'mobilecompanyphones': '',
                         'message': 'you are not authorized user'})
