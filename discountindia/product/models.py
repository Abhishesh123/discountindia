from django.db import models


# Create your models here.
class TimeStampModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class MainProduct(TimeStampModel):
    # mobile
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=150, null=True, blank=True)

    def __str__(self):
        return self.name


class Product(TimeStampModel):
    # company ,mi
    main_product = models.ForeignKey(MainProduct, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=150, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='product/images', null=True, blank=True)
    brand = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class SubPoduct(TimeStampModel):
    # note 6 pro
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    price = models.CharField(max_length=200, null=True, blank=True)
    slug = models.SlugField(max_length=150, null=True, blank=True)
    brand = models.BooleanField(default=False)
    popular = models.BooleanField(default=False)
    upcoming = models.BooleanField(default=False)
    new = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class SubProductImages(TimeStampModel):
    subproudct = models.ForeignKey(SubPoduct, on_delete=models.CASCADE, related_name='subproductimage')
    image = models.ImageField(upload_to='subproduct/images', null=True, blank=True)
    profile = models.BooleanField(default=False)

    def __str__(self):
        return self.subproudct.name


class SubProductOptions(TimeStampModel):
    option_name = models.CharField(max_length=200)

    def __str__(self):
        return self.option_name


class SubProductSpecification(TimeStampModel):
    suproduct = models.ForeignKey(SubPoduct, on_delete=models.CASCADE)
    product_options = models.ForeignKey(SubProductOptions, on_delete=models.CASCADE)
    value = models.TextField()

    def __str__(self):
        return self.suproduct.name


class AnonymousToken(TimeStampModel):
    key = models.CharField(max_length=200)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.key
