from django.urls import path
from .views import *

urlpatterns = [
    path('main-products/', MainProductApi.as_view(), name='mainproudct'),
    path('mobile/companies/', MobileCompanies.as_view(), name='mobilecomapny'),
    path('mobile/companies-phones-new/', MobileCompaniesPhonesNew.as_view(), name='mobilecomapniesphonesnew'),
    path('mobile/companies-phones-popular/', MobileCompaniesPhonesPopular.as_view(),
         name='mobilecomapniesphonespopular'),
    path('mobile/companies-phones-upcoming/', MobileCompaniesPhonesUpcoming.as_view(),
         name='mobilecomapniesphonesupcoming'),
    # ---------------------***************************-------------------------- #
    path('laptop/companies', LaptopCompanies.as_view(), name='laptopcompanies'),
    path('laptop/companies-laptop-new/', NewLaptops.as_view(), name='newlaptops'),
    path('laptop/companies-laptop-popular/', PopularLaptops.as_view(), name='popularlaptops'),
    path('laptop/companies-laptop-upcoming/', UpcomingLaptops.as_view(), name='upcominglaptops'),
    path('tvs/popular-tvs/', PopularTvs.as_view(), name='populartvs'),
    # ---------------------***************************-------------------------- #

]
