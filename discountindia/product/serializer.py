from rest_framework import serializers
from .models import *


class MainProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = MainProduct
        fields = ("name", "slug")


class MobileCompaniesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ("name", "image", "slug")


class MobileCompaniesPhonesSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField('subproductimage')

    class Meta:
        model = SubPoduct
        fields = ("name", "price", "image", "slug")

    def subproductimage(self, obj):
        img = SubProductImages.objects.filter(subproudct=obj.pk, profile=True).first()
        return img.image.url